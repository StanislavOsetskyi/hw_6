<?php
require_once '../Config/db.php';


class Student extends Person
{

    public function __construct($fullName,$phone,$email,$role,$averageMark,$subject,$workingDay)
    {
        parent::__construct($fullName,$phone,$email,$role,$averageMark,$subject,$workingDay);

    }

    public function getVisitCard()
    {
        $str = parent::getVisitCard();
        $str .= $this->averageMark;
        return $str;
    }
}
try{
    $sql = 'SELECT * FROM members';
    $pdoResult = $pdo->query($sql);
    $membersArr = $pdoResult->fetchAll();
}catch(Exception $exception){
    echo "Error getting members " . $exception->getCode() . ' ' . $exception->getMessage();
    die();
}
