<?php
require_once '../Config/db.php';

class Person
{
    protected $id;
    protected $fullName = '';
    protected $phone = 1;
    protected $email = '';
    protected $role = '';
    protected $averageMark = 1.1 ;
    protected $subject = '';
    protected $workingDay = '';

    public function __construct($fullName,$phone,$email,$role,$averageMark,$subject,$workingDay)
    {
        $this->fullName = htmlspecialchars($fullName);
        $this->phone = htmlspecialchars($phone);
        $this->email = htmlspecialchars($email);
        $this->role = htmlspecialchars($role);
        $this->averageMark = htmlspecialchars($averageMark);
        $this->subject = htmlspecialchars($subject);
        $this->workingDay = htmlspecialchars($workingDay);

    }

    static public function getById($id, PDO $pdo){
        $id = htmlspecialchars($id);
        try {
            $sql = 'SELECT * FROM members WHERE id=:id';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id',$id);
            $statement->execute();
            $membersArr = $statement->fetchAll();
            $memberArr = $membersArr[0];
            /*echo '<pre>';
            print_r($memberArr);
            die();*/
            $memberObj = new self($memberArr['full_name'],$memberArr['phone'],$memberArr['email'],
                $memberArr['role'],$memberArr['average_mark'],$memberArr['subject'],$memberArr['working_day']);
            $memberObj->setId($memberArr['id']);
            return $memberObj;

        }catch (Exception $exception){
            echo "Error getting products! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function destroy($id, PDO $pdo){
        $member = self::getById($id,$pdo);
        $member->delete($pdo);
    }

    public function getVisitCard(){
        $str = '';
        $str.= $this->fullName. ',';
        $str.= $this->phone. ',';
        $str.= $this->email. ',';
        $str.= $this->role. ',';
        return $str;
    }

    public function store(PDO $pdo){
        try{
            $sql = 'INSERT INTO members SET 
            full_name = :pdo_full_name,
            phone = :pdo_phone,
            email = :pdo_email,
            role = :pdo_role,
            average_mark = :pdo_average_mark,
            subject = :pdo_subject,
            working_day = :pdo_working_day';

            $statement = $pdo->prepare($sql);
            $statement->bindValue(':pdo_full_name', $this->fullName);
            $statement->bindValue(':pdo_phone', $this->phone);
            $statement->bindValue(':pdo_email', $this->email);
            $statement->bindValue(':pdo_role', $this->role);
            $statement->bindValue(':pdo_average_mark', $this->averageMark);
            $statement->bindValue(':pdo_subject', $this->subject);
            $statement->bindValue(':pdo_working_day', $this->workingDay);
            $statement->execute();

        }catch (Exception $exception){
            echo 'Error storing member' .''. $exception->getMessage();
            die();
        }
    }

    public function delete(PDO $pdo){
        try {
            $sql = "DELETE FROM members WHERE id=:id";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id',$this->id);
            $statement->execute();
        }catch (Exception $exception){
            echo "Error deleting member! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function setId($id)
    {
        $this->id = $id;
    }


}