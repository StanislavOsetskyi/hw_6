<?php
if(empty($_POST['member_id'])){
    header('Location:index.php');
}

require_once '../config/db.php';
require_once '../Classes/Person.php';

Person:: destroy($_POST['member_id'], $pdo);

header('Location:index.php');